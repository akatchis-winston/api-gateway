package io.codekrijger.winston.gateway.api.rest.controllers;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/resolve")
public class QueryResolverRestController
{
    @HystrixCommand(fallbackMethod = "resolveQueryFallback")
    @PostMapping
    public String resolveQuery(@RequestParam(value = "query", required = true) String query)
    {
        return query;
    }

    public String resolveQueryFallback()
    {
        return "failed";
    }
}
